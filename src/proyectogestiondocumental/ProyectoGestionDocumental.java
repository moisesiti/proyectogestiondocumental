/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectogestiondocumental;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author andy
 */
public class ProyectoGestionDocumental extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
         stage.initStyle(StageStyle.TRANSPARENT);
        
        //Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        Parent root = FXMLLoader.load(getClass().getResource("vista/Splash.fxml"));
       // Parent root = FXMLLoader.load(getClass().getResource("vista/Principal.fxml"));
        
        Scene scene = new Scene(root);
        scene.setFill(null);
        stage.setScene(scene);
      
        //rutaIconofuncional: jar:file:/home/andy/Documentos/DESARROLLO/NetBeansProjects/STDXML/dist/run344369263/STDXML.jar!/stdxml/recursos/icono-stdxml250.png
        //rutaIcono: jar:file:/home/andy/Documentos/DESARROLLO/NetBeansProjects/SistemaGestionDocumentos/dist/run1642335870/SistemaGestionDocumentos.jar!/sistemagestiondocumentos/recursos/carpeta.png
        stage.setTitle("Sistema de Gestión de Documentos");
        System.out.println("rutaIcono: "+getClass().getResource("/proyectogestiondocumental/recursos/carpeta.png").toExternalForm());
        
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/proyectogestiondocumental/recursos/carpeta.png")));
        
        
        //stage.setResizable(false);
        //stage.sizeToScene();
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.getProperties().entrySet()
                              .stream()
                              .filter(linea -> 
                                      linea.getKey().toString().startsWith("os.name") ||
                                      linea.getKey().toString().startsWith("os.version") ||
                                      linea.getKey().toString().startsWith("user.home") ||        
                                      linea.getKey().toString().startsWith("java.class.path") ||
                                      linea.getKey().toString().startsWith("file.separator")        
                               )                             
                              .forEach(System.out::println);
        System.out.println("------------------------------------------------------");
        launch(args);
    }
    
}
