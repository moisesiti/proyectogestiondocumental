/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectogestiondocumental.controlador;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import proyectogestiondocumental.modelo.Archivo;
import proyectogestiondocumental.modelo.ArchivoImpl;
import proyectogestiondocumental.modelo.ArchivoPDF;
import proyectogestiondocumental.modelo.ArchivoPDFImpl;
import proyectogestiondocumental.modelo.ArchivoWord;
import proyectogestiondocumental.modelo.ArchivoWordImpl;
import proyectogestiondocumental.modelo.Configuraciones;
import proyectogestiondocumental.modelo.ConfiguracionesImpl;
import proyectogestiondocumental.modelo.Multimedia;
import proyectogestiondocumental.modelo.MultimediaImpl;
import proyectogestiondocumental.modelo.Ventana;
import proyectogestiondocumental.modelo.VentanaImpl;

/**
 * FXML Controller class
 *
 * @author andy
 */
public class PrincipalController implements Initializable {
    private ArchivoPDF archivoPDF;
    private Multimedia multimedia;
    private Configuraciones configuraciones;
    private Ventana ventana;
    private Archivo archivo;
    private ArchivoWord archivoWord;



    // Variables auxiliares
    private String tipo;
    private Path directorioBase;
    private Path directorioDestino;
    private Optional<Path> rutaArchivo;
    private List<Path> archiDir;
    private final String FORMATO = "%s-%s-%s%s";

    @FXML
    private AnchorPane base;

    @FXML
    private ScrollPane contenedor;

    @FXML
    private TextField paginas;

    @FXML
    private Button atras;

    @FXML
    private Button avanzar;

    @FXML
    private Button renombrar;

    @FXML
    private MenuItem ocrVisual;

    @FXML
    private TextField primerTexto;

    @FXML
    private TextField segundoTexto;

    @FXML
    private TextField tercerTexto;

    @FXML
    private TextField renombradoCompleto;


    @FXML
    private TreeView<String> arbolVisual;


    private void refrezcar(int numero){
        Optional.ofNullable(tipo).ifPresent(tipoArchivo->{
            switch(tipoArchivo.split("/")[1]){
                case "pdf":System.out.println("numero: "+(numero-1));

                    Image imagen = archivoPDF.conversionPdf2Imagen(numero-1);
                    configuraciones.actualizarImagen(imagen);

                    //System.out.println("TextoPDF("+numero+")->"+archivoPDF.extraerTexto(numero));
                    break;
                default:
                    break;
            }
        });
    }


    @FXML
    void AVANZAR(ActionEvent event) {
        System.out.println("adelante");
        int[] numeracion = configuraciones.nroPagina("+");
        paginas.setText(numeracion[0]+"/"+numeracion[1]);
        this.refrezcar(numeracion[0]);

    }

    @FXML
    void ATRAS(ActionEvent event) {
        System.out.println("atras");
        int[] numeracion = configuraciones.nroPagina("-");
        paginas.setText(numeracion[0]+"/"+numeracion[1]);
        this.refrezcar(numeracion[0]);
    }
    
    private void elementoSeleccionado(){
        rutaArchivo = Optional.ofNullable(
                Paths.get(directorioBase.toString(), arbolVisual.getSelectionModel().getSelectedItem().getValue())
        );
    }

    @FXML
    void ABRIR_ARCHIVO(ActionEvent event) {
//       rutaArchivo = null;
//       rutaArchivo = //Optional.ofNullable(ventana.seleccionarArchivo("Seleccione el archivo de interes"));
//                       Optional.ofNullable(ventana.seleccionarArchivo("Seleccione el archivo de interes"));
        // Archivo de interes seleccionado
        this.elementoSeleccionado();

        System.out.println("Seleccionado: "+rutaArchivo.get().toString());
        System.out.println("Presente: "+rutaArchivo.isPresent());
        System.out.println("Regular: "+Files.isRegularFile(rutaArchivo.get()));

        if(rutaArchivo.isPresent() && Files.isRegularFile(rutaArchivo.get())){
           System.out.println("cargando archivo: "+rutaArchivo.get().toString());

           // Identificando el tipo de archivo
           multimedia.cargarArchivo(rutaArchivo.get());
           tipo = multimedia.tipoArchivo();
           String estructura[] = tipo.split("/");
           System.out.println("estructura: "+estructura[0]+"\n"+estructura[1]);

           // Garantizando cerrar cualquier buffer de memoria
           archivoPDF.cerrarPDF();
           configuraciones.detenerReproduccion();



           // Operacion en funcion al tipo de archivo a tratar
           switch(estructura[0]){
               case "application":
                   if(estructura[1].startsWith("pdf")){ System.out.println("Analizando PDF");
                        archivoPDF.extraerPdf(rutaArchivo.get());
                        int totalPaginas = archivoPDF.numeroDePaginas();
                        configuraciones.establecerPaginacion(totalPaginas);
                        paginas.setText("1/"+totalPaginas);
                        Image imagen = archivoPDF.conversionPdf2Imagen(0);
                        configuraciones.visorImagenes();
                        configuraciones.actualizarImagen(imagen);

                   }
                   if(estructura[1].startsWith("vnd.openxmlformats-officedocument.wordprocessingml.document") ||
                      estructura[1].startsWith("msword") ){ System.out.println("Analizando WORD");
                        archivoWord.extraerMSWord(rutaArchivo.get());
                        configuraciones.notepad();
                        configuraciones.actualizarNotepad(archivoWord.contenidoExtraido());
                   }
                   break;
               case "image":
                   paginas.setText("1/1");
                   contenedor.setContent(null);
                   configuraciones.visorImagenes();

                   FileInputStream is = null;
                   try {
                       is = new FileInputStream(rutaArchivo.get().toFile());
                   } catch (FileNotFoundException ex) {
                        Logger.getLogger(PrincipalController.class.getName()).log(Level.SEVERE, null, ex);
                   }

                   //Image cargaImagen = new Image(rutaArchivo.get().toUri().toString());
                   Image cargaImagen = new Image(is);
                   configuraciones.actualizarImagen(cargaImagen);

                    // Cerrando flujo de imagen
                    try {
                        is.close();
                    } catch (IOException ex) {
                        Logger.getLogger(PrincipalController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                   break;
               case "text":
                   configuraciones.notepad();
                   AtomicReference<String> texto = new AtomicReference<>("");
                   Stream<String> lineasTXT = archivo.leerAchivoExterno(rutaArchivo.get().toString());
                   lineasTXT.forEach(linea->{
                                texto.set(texto.get()+"\n"+linea);
                            });
                   configuraciones.actualizarNotepad(texto.get());
                   paginas.setText("1/1");
                   lineasTXT.close();
                   break;
               case "video":
                   paginas.setText("1/1");
                   configuraciones.reproductor();
                   configuraciones.actualizarVideo(rutaArchivo.get());
                   break;
               default:
                   System.out.println("Archivo no valido...!!!");
                   break;

           }


       }else{
           System.out.println("Archivo no seleccionado");
       }



    }

    @FXML
    void MOVER_ARCHIVO(ActionEvent event) {
        this.elementoSeleccionado();
        if(rutaArchivo.isPresent() && Files.isRegularFile(rutaArchivo.get())){
         String respuesta =
                 ventana.confirmacion("Confirmación",
                                      "Desea mover este archivo: "+rutaArchivo.get().getFileName(),
                                      "Si",
                                      "No");

         if(respuesta.equalsIgnoreCase("Si")){
             System.out.println("proceso de movimiento de archivo");
             this.liberandoComponentesActivos();
             
              String seleccionRutaDestino = ventana.seleccionarDirectorio("Selecciones directorio Destino", directorioDestino);
              if( seleccionRutaDestino != null ){ System.out.println("proceso de movimiento iniciado");
                 try {
                     
                     Files.move(
                             rutaArchivo.get(),
                             Paths.get(seleccionRutaDestino, rutaArchivo.get().getFileName().toString()),
                             StandardCopyOption.REPLACE_EXISTING);
                     this.refrezcandoSistemaArchivosVisual();
                     directorioDestino = Paths.get(seleccionRutaDestino);
                     
                     ventana.mensajeria("INFORMACIÓN", "El archivo fue colocado en la nueva direccion.");
                 } catch (IOException ex) {
                     ventana.mensajeria("Error", "No se pudo mover el archivo a la nueva direccion.");
                     Logger.getLogger(PrincipalController.class.getName()).log(Level.SEVERE, null, ex);
                 }
                
              }else{
               ventana.mensajeria("INFORMACIÓN", "Debe seleccionar un directorio primero.");
              }
             
         }else{
           ventana.mensajeria("INFORMACIÓN", "El proceso fue cancelado.");
         }
       }else{
           ventana.mensajeria("INFORMACIÓN", "Debe seleccionar un archivo primero.");
       }
        
    }

    @FXML
    void ELIMINAR_ARCHIVO(ActionEvent event) {
       this.elementoSeleccionado();
       
       if(rutaArchivo.isPresent() && Files.isRegularFile(rutaArchivo.get())){
         String respuesta =
                 ventana.confirmacion("Confirmación",
                                      "Desea eliminar el archivo seleccionado: "+rutaArchivo.get().getFileName(),
                                      "Si",
                                      "No");

         if(respuesta.equalsIgnoreCase("Si")){
             System.out.println("archivo eliminado");
             this.procesoEliminacionArchivo();
             ventana.mensajeria("INFORMACIÓN", "El archivo fue eliminado.");
         }else{
           ventana.mensajeria("INFORMACIÓN", "El proceso fue cancelado.");
         }
       }else{
           ventana.mensajeria("INFORMACIÓN", "Debe seleccionar un archivo primero.");
       }

    }

    @FXML
    void TREE_CLIQUEADO(MouseEvent event) {
        if(event.getButton() == MouseButton.PRIMARY){
           if(event.getClickCount() >= 2){
               // Verificando si el doble click es sobre la raiz o cualquier elemento
               if(arbolVisual.getSelectionModel().getSelectedIndex() == 0){

                 directorioBase = Paths.get( directorioBase.getParent().toAbsolutePath().toUri() ); // Nuevo directorio base
                 System.out.println("archivoBase: "+directorioBase);
                 archiDir = archivo.listarDirectoriosyArchivosRapido(directorioBase.toString());
                 configuraciones.establecerDirectorioActual(archiDir);

               }else{

                rutaArchivo = Optional.ofNullable(
                    Paths.get(directorioBase.toString(), arbolVisual.getSelectionModel().getSelectedItem().getValue())
                );

                // Si el elemento seleccionado es un directorio.
                Path rutaAnalizada = archivo.normalizarRuta(rutaArchivo.get().toString());
                if(Files.isDirectory(rutaAnalizada) && Files.exists(rutaAnalizada)){

                     directorioBase = rutaAnalizada; // Nuevo directorio base
                     System.out.println("archivoBaseHoja: "+directorioBase);
                     archiDir = archivo.listarDirectoriosyArchivosRapido(directorioBase.toString());
                     configuraciones.establecerDirectorioActual(archiDir);

                }

               }

           };
        }
    }

    private void rellenarCampoTexto(){
        renombradoCompleto.setText(String.format(
                                    FORMATO,
                                    primerTexto.getText(),
                                    segundoTexto.getText(),
                                    tercerTexto.getText(),
                                    archivo.extraerExtension(rutaArchivo.get())
                                  )
        );
    }
    @FXML
    void ACCION_PRIMER_TEXTO(ActionEvent event) {
        System.out.println("presente: "+rutaArchivo.isPresent());
        if(rutaArchivo.isPresent()) {
            this.rellenarCampoTexto();
            renombrar.setDisable(false);
        }

    }

    @FXML
    void ACCION_SEGUNDO_TEXTO(ActionEvent event) {
        System.out.println("presente: "+rutaArchivo.isPresent());
        if(rutaArchivo.isPresent()) {
            this.rellenarCampoTexto();
            renombrar.setDisable(false);
        }
    }

    @FXML
    void ACCION_TERCER_TEXTO(ActionEvent event) {
       System.out.println("presente: "+rutaArchivo.isPresent());
        if(rutaArchivo.isPresent()) {
            this.rellenarCampoTexto();
            renombrar.setDisable(false);
        }
    }

    private void liberandoComponentesActivos(){
        // Garantizando cerrar cualquier buffer de memoria
        archivoPDF.cerrarPDF();
        configuraciones.detenerReproduccion();

        paginas.setText("1/1");
        contenedor.setContent(null);
    }
    
    private void refrezcandoSistemaArchivosVisual(){
        System.out.println("Actualizando Directorio: "+directorioBase.toString());
        archiDir = null;
        archiDir = archivo.listarDirectoriosyArchivosRapido(directorioBase.toString());
        configuraciones.establecerDirectorioActual(archiDir);
    }
    
    private void procesoEliminacionArchivo(){
        this.liberandoComponentesActivos();

        // Borrando el archivo original despues de renombrar
        System.out.println("Preparando para borrar: "+rutaArchivo.get());
        try {
            if(Files.deleteIfExists(rutaArchivo.get())){ // Solo si se borra el archivo de origen, se actualiza la vista de archivos
                System.out.println("Archivo borrado");
                this.refrezcandoSistemaArchivosVisual();
            }
        } catch (IOException ex) {
            Logger.getLogger(PrincipalController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    void RENOMBRAR_ARCHIVO(ActionEvent event) {
        System.out.println("Renombrando Archivo");

        String seleccionRutaDestino = ventana.seleccionarDirectorio("Selecciones directorio Destino", directorioDestino);
        System.out.println("archivo "+seleccionRutaDestino);

        if( seleccionRutaDestino != null ){ System.out.println("proceso de copiado iniciado");
            String respuesta =
                ventana.confirmacion("Confirmación",
                                     "Desea guardar el archivo en la ruta seleccionada:",
                                     "De Acuerdo...!!!",
                                     "Todavía no");

            if(respuesta.equalsIgnoreCase("De Acuerdo...!!!")){
                directorioDestino = Paths.get(seleccionRutaDestino);
                archivo.copiarArchivo(rutaArchivo.get(), directorioDestino.resolve(renombradoCompleto.getText()));
                ventana.mensajeria("INFORMACIÓN", "Proceso de renombrado fue exitoso...!!!");

                primerTexto.setText(null);
                segundoTexto.setText(null);
                tercerTexto.setText(null);
                renombradoCompleto.setText("Primer Texto - Segundo Texto - Tercer Texto.Extension");

                renombrar.setDisable(true);

                this.procesoEliminacionArchivo();

            }else{
                ventana.mensajeria("INFORMACIÓN", "El proceso fue cancelado.");
            }

        }

    }

    @FXML
    void SALIR(ActionEvent event) {
        Platform.exit();
        System.exit(0);
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        // Instanciando las clases de interes

        multimedia = new MultimediaImpl();
        archivoPDF = new ArchivoPDFImpl();
        configuraciones = new ConfiguracionesImpl();
        ventana = new VentanaImpl();
        archivo = new ArchivoImpl();
        archivoWord = new ArchivoWordImpl();

        rutaArchivo = Optional.ofNullable(null);

        configuraciones.inicializacion(contenedor, base);
        configuraciones.inicializarArbolVisual(arbolVisual);

        directorioBase = Paths.get(System.getProperty("user.home"));
                        //Paths.get("/home/andy/Documentos");
        directorioDestino = directorioBase;

        archiDir = archivo.listarDirectoriosyArchivosRapido(directorioBase.toString());
        configuraciones.establecerDirectorioActual(archiDir);



    }

}
