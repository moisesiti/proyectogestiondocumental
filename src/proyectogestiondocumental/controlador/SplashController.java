/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectogestiondocumental.controlador;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import proyectogestiondocumental.modelo.Ventana;
import proyectogestiondocumental.modelo.VentanaImpl;

/**
 * FXML Controller class
 *
 * @author andy
 */
public class SplashController implements Initializable {
    @FXML
    private AnchorPane panel;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Thread hilo = new Thread(new Runnable() {
            @Override
            public void run() {
                
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(SplashController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                    Platform.runLater(new Runnable() {
                         @Override
                         public void run() {
                             Ventana ventana = new VentanaImpl();
                             ventana.nuevaVentana("/proyectogestiondocumental/vista/Principal.fxml");
                             
                             ventana.cerrarVentana(panel);

                         }
                     });
            }
        });
      
      hilo.setName("SplashScreen 1");
      hilo.start();
    }    
    
}
