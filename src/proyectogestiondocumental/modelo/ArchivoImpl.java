/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectogestiondocumental.modelo;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author andy
 */
public class ArchivoImpl implements Archivo{

    public ArchivoImpl() {
    }
    
    @Override
    public Path rutaNormalizadaLocal(String nombreArchivo) throws MalformedURLException, URISyntaxException{
        String rutaOriginal =  getClass().getResource("ArchivoImpl.class").getFile();
        String rutaReal = rutaOriginal.substring(0, rutaOriginal.indexOf("SistemaGestionDocumentos.jar")).concat(nombreArchivo);
        URL urlNueva = new URL(rutaReal);
                
       
        return Paths.get(urlNueva.toURI());    
    }
   
    private Path archvivosEspecialesWindows(Path ruta){
        /*
        C:\Users\Rosa\Desktop
        C:\Users\Rosa\Documents
        C:\Users\Rosa\Pictures\image
        C:\Users\Rosa\Music
        C:\Users\Rosa\Videos
        C:\Program Files
        C:\Windows\system
        C:\Users
        */
        
        
        String nombreArchivo = ruta.getFileName().toString();
        System.out.println("nombreArchivo: "+nombreArchivo);
        switch(nombreArchivo){
            case "Mis documentos":
                ruta = Paths.get(System.getProperty("user.home"),"Documents");
                break;
            case "Escritorio":
                ruta = Paths.get(System.getProperty("user.home"),"Desktop");
                break;
            case "Mis imágenes":
                ruta = Paths.get(System.getProperty("user.home"),"Pictures");
                break;    
            case "Mi música":
                ruta = Paths.get(System.getProperty("user.home"),"Music");
                break;
            case "Mis vídeos":
                ruta = Paths.get(System.getProperty("user.home"),"Videos");
                break;    
            default:
                break;
            
        }
        
        System.out.println("Ruta Especial Salida: "+ruta);
        return ruta;
    }
    
    @Override
    public Path normalizarRuta(String rutaActual){
        Path rutaModificada = null;
        URL urlNueva = null;
       
        try {
            urlNueva = Paths.get(rutaActual).toUri().toURL();
            //System.out.println("rutaExterna1: "+urlNueva.getPath());
            
            rutaModificada = Paths.get(urlNueva.toURI());
            //System.out.println("rutaExterna2: "+ruta.toString());
            
            // Solo para ser usado cuando el sistema operativo sea windows
            if(System.getProperty("os.name").toLowerCase().contains("windows"))
                rutaModificada = this.archvivosEspecialesWindows(rutaModificada);
            
        } catch (MalformedURLException | URISyntaxException ex) { 
            Logger.getLogger(ArchivoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return rutaModificada;
    }
    
    @Override
    public Stream<String> leerAchivoLocal(String nombreArchivo) {
        Stream<String> lineas = null;
        Path ruta = null;
          
        try {
            
            ruta = this.rutaNormalizadaLocal(nombreArchivo);
             System.out.println("path: "+ruta.toString());
    
            lineas = Files.lines(ruta, Charset.forName("UTF-8"));
           
            
        } catch (IOException | URISyntaxException ex) {
            Logger.getLogger(ArchivoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
          // if(lineas != null) lineas.close();
        }
        
        return lineas;
    }
    
    @Override
    public Stream<String> leerAchivoExterno(String rutaArchivo) {
        Stream<String> lineas = null;
        
        try {
            lineas = Files.lines(this.normalizarRuta(rutaArchivo), Charset.forName("UTF-8"));
        } catch (IOException ex) {
            Logger.getLogger(ArchivoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return lineas;
    }

    @Override
    public Stream<Path> listarDirectoriosyArchivos(String rutaBase) {
        Stream<Path> directorios = null;
        
        try {
            Path rutaNormalizada = this.normalizarRuta(rutaBase);
            System.out.println("Privilegios: -> Lectura = "+Files.isReadable(rutaNormalizada));
           
            directorios = Files.walk(rutaNormalizada);
            
        } catch (IOException ex) {
            Logger.getLogger(ArchivoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return directorios;
    }
    
    

    @Override
    public List<Path> listarDirectoriosyArchivosRapido(String rutaBase) {
        Stream<Path> directorios = null;
        
        try {
            Path rutaNormalizada = this.normalizarRuta(rutaBase);
            System.out.println("Privilegios: -> Lectura = "+Files.isReadable(rutaNormalizada));
           
            directorios = Files.walk(rutaNormalizada, 1, FileVisitOption.FOLLOW_LINKS);
            
            
        } catch (IOException ex) {
            Logger.getLogger(ArchivoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
      
        
        return directorios.collect(Collectors.toList());
    }
    
    

    @Override
    public void copiarArchivo(Path archivoOriginal, Path rutaArchivo) {
        
        try {
            System.out.println("copia: "+Files.copy(archivoOriginal, rutaArchivo, StandardCopyOption.REPLACE_EXISTING));
                  
        } catch (IOException ex) {
            Logger.getLogger(ArchivoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    @Override
    public Path verificarDuplicidadDirectorio(Path ruta) {
      Path salida = ruta;
       // System.out.println("verificar: "+ruta);
       // System.out.println("repetido: "+Files.exists(ruta));
      if(Files.exists(ruta)){
           LocalDateTime ldt = LocalDateTime.now();
           String fechaHora = ldt.toString() // 2018-11-14T13:06:42.703
                                 .replaceAll("-", "")
                                 .replaceAll(":", "")
                                 .replace("T", "-")
                                 .replace(".", "");
         salida = Paths.get(ruta.toString().concat("(").concat(fechaHora).concat(")"));
      }
       
      return salida;
    }

    private Path verificarArchivoExistente(Path archivoActual){
        Path salida = archivoActual;
        
        
        if(Files.exists(archivoActual)){
           String nombreArchivo = archivoActual.getFileName().toString();
           // System.out.println("existe: "+archivoActual.getFileName()); 
           
           
           if(nombreArchivo.contains("(")){ // se verifica si el existente ya fue numerado anteriormente para buscar la secuencia
              String numero = nombreArchivo.substring(nombreArchivo.indexOf("(")+1, nombreArchivo.indexOf(")"));
             //  System.out.println("numero: "+numero);
              int indice = Integer.valueOf(numero);
              
              String nuevoNombre = nombreArchivo.replace("("+indice+")", "("+(indice+1)+")");
              Path nuevaRuta = archivoActual.getParent().resolve( nuevoNombre );
              
            //   System.out.println("traia numeracion: nuevo: "+nuevaRuta);
              
               salida = this.verificarArchivoExistente(nuevaRuta);
                      
           }else{ // si no ha sido numerado se agrega un numero
                 
             salida = this.verificarArchivoExistente(
                     archivoActual.getParent().resolve(archivoActual.getFileName().toString().replace(".", "(1)."))
             );
             
            // System.out.println("agregandoNumeracion-> "+salida);
           }        
            
         //  System.out.println("duplicadoCorregido:: "+salida);
        }
       
        
       return salida;
    }
    
    @Override
    public void crearNuevoArchivoSecuencial(Path archivoFuente, Path archivoDestino){
        archivoDestino = this.verificarArchivoExistente(archivoDestino);
        
        try {
            System.out.println("destino: "+archivoDestino);
            Files.copy(archivoFuente, archivoDestino, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            Logger.getLogger(ArchivoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public Path fusionRutas(Path directorioBase, Path directorioOrigen, Path directorioDestino) {
       return Paths.get(
               directorioDestino.toString(), 
               directorioBase.subpath(
                       directorioOrigen.getNameCount()-1, 
                       directorioBase.getNameCount() 
               ).toString()
       );
    }
    
    

    @Override
    public Path verificarDirectorioBaseDestino(Path directorioBase, Path directorioOrigen, Path directorioDestino) {
       Path fusion = this.fusionRutas(directorioBase, directorioOrigen, directorioDestino); 
       Path nuevoDirectorioBase = this.verificarDuplicidadDirectorio(fusion);
       
       if(fusion.compareTo(nuevoDirectorioBase) == 0 ) 
           return null;
       else
           return nuevoDirectorioBase;          
    }

    @Override
    public void escribirArchivo(Path rutaDestino, List<String> lineas) {
       
        try {
             if(Files.exists(rutaDestino)){ // escribe al final
                    Files.write(rutaDestino, lineas, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
             }else{ // escribe todo el archivo
                   Files.write(rutaDestino, lineas, Charset.forName("UTF-8"), StandardOpenOption.WRITE);
             }
             
        } catch (IOException ex) {
                Logger.getLogger(ArchivoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }

    @Override
    public String extraerExtension(Path ruta) {
        String nombreArchivo = ruta.getFileName().toString();
        String extension = null;
        
        extension = nombreArchivo.substring( nombreArchivo.indexOf("."), nombreArchivo.length());
        
        return extension;
    }
    
    
    
    
}