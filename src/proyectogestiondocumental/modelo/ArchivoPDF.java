/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectogestiondocumental.modelo;

import java.nio.file.Path;
import javafx.scene.image.Image;

/**
 *
 * @author andy
 */
public interface ArchivoPDF {
    
    /**
     @param ruta Se refiere al lugar donde se encuentra el archivo de interes*/
    public void extraerPdf(Path ruta);
    /**@param pagina Pagina que se quiere visualizar*/
    public Image conversionPdf2Imagen(int pagina);
    
    public String extraerTexto(int pagina);
    public int numeroDePaginas();
    public void cerrarPDF();
    
    
}
