/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectogestiondocumental.modelo;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.text.PDFTextStripper;

/**
 *
 * @author andy
 */
public class ArchivoPDFImpl implements ArchivoPDF{
    private PDDocument documento;
    private PDFRenderer renderer;

    
    
    private void renderizarPDF(){
       renderer = new PDFRenderer(documento);
       
    }
    
    @Override
    public void extraerPdf(Path ruta) {
        System.setProperty("sun.java2d.cmm", "sun.java2d.cmm.kcms.KcmsServiceProvider");
        try {
            
            documento = PDDocument.load(ruta.toFile());
            this.renderizarPDF();
        } catch (IOException ex) {
            System.out.println("Error al abrir el documento: "+ruta);
            Logger.getLogger(ArchivoPDFImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String extraerTexto(int pagina) {
         PDFTextStripper pdfText = null;
         String textoSalida = null;
         
        try {
           pdfText = new PDFTextStripper();
           pdfText.setStartPage(pagina);
           pdfText.setEndPage(pagina);
           textoSalida = pdfText.getText(documento);
           
        } catch (IOException ex) {
            Logger.getLogger(ArchivoPDFImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return textoSalida;
    }
    
    
    
    @Override
    public Image conversionPdf2Imagen(int pagina){
        BufferedImage bufferImagen = null;
        Image imagenPDF = null;
        
        try {
            // the scaling factor, where 1 = 72 DPI
            bufferImagen = renderer.renderImage( pagina, 2.5f );//renderer.renderImageWithDPI(pagina, (float) 300.0, ImageType.ARGB);
            //System.out.println("dimensiones: "+bufferImagen.getWidth()+"-"+bufferImagen.getHeight());
            
            //ImageIO.write(bufferImagen, "JPEG", new File("/home/andy/Documentos/ArchivosXML/CARPETA3/imagenpdf.jpg"));
            
            imagenPDF = SwingFXUtils.toFXImage(bufferImagen, null);
            
            //imagenPDF = new Image(new FileInputStream("/home/andy/Documentos/ArchivosXML/CARPETA3/imagenpdf.jpg"));
            System.out.println("imagen Dimension: ancho:"+imagenPDF.getWidth()+" alto:"+imagenPDF.getHeight());
        } catch (IOException ex) {
            System.out.println("No se pudo extraer la renderizacion de la pagina: "+pagina);
            Logger.getLogger(ArchivoPDFImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
       
      return imagenPDF; 
    }

    @Override
    public int numeroDePaginas() {
        return documento.getNumberOfPages();
    }

    @Override
    public void cerrarPDF() {
         try {
                if(documento != null)
                    documento.close();
            } catch (IOException ex) {
                System.out.println("No se pudo cerrar el pdf");
                Logger.getLogger(ArchivoPDFImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    
    
    
}
