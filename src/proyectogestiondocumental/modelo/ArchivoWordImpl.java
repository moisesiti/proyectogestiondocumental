/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectogestiondocumental.modelo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

/**
 *
 * @author andy
 */
public class ArchivoWordImpl implements ArchivoWord{
    private String textoRecuperado;

    private String leerArchivoDOC(Path ruta){ System.out.println("DOC: "+ruta);
        String texto = null;
        InputStream fis = null;
        WordExtractor extractor = null; 
        
        try {
            fis = new FileInputStream(ruta.toFile());
            extractor = new WordExtractor(fis);
            texto = extractor.getText();
            
            extractor.close();
        } catch (IOException ex) {
            Logger.getLogger(ArchivoWordImpl.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            try {
                if(extractor != null)
                    extractor.close();
            } catch (IOException ex) {
                Logger.getLogger(ArchivoWordImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
        return texto;
    }
    
    private String leerArchivoDOCX(Path ruta){ System.out.println("DOCX: "+ruta);
        String texto = null;
        FileInputStream fis = null;
            try {
                fis = new FileInputStream(ruta.toFile());
                XWPFDocument documento = new XWPFDocument(fis);
                XWPFWordExtractor extractor = new XWPFWordExtractor(documento);
                
                texto = extractor.getText();
                
                documento.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ArchivoWordImpl.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ArchivoWordImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    if(fis != null)
                        fis.close();
                } catch (IOException ex) {
                    Logger.getLogger(ArchivoWordImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
       return texto;     
    }
    
    @Override
    public void extraerMSWord(Path ruta) {
       
        if(ruta.toString().endsWith("docx")){ 
           textoRecuperado = this.leerArchivoDOCX(ruta);
        }else{
           textoRecuperado = this.leerArchivoDOC(ruta);
        }
        
        System.out.println("texto: "+textoRecuperado);
       
    }
    
    @Override
    public String contenidoExtraido(){
        return textoRecuperado;
    }
    
}
