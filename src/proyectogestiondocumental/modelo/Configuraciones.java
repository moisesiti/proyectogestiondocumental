/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectogestiondocumental.modelo;

import java.nio.file.Path;
import java.util.List;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

/**
 *
 * @author andy
 */
public interface Configuraciones {
    
    public void inicializacion(ScrollPane contenedor, Pane base);
    public void visorImagenes();
    public void actualizarImagen(Image imagen);
    public void establecerPaginacion(int paginasTotales);
    public int[] nroPagina(String texto);
    
    public void notepad();
    public void actualizarNotepad(String texto);
    
    public void reproductor();
    public void actualizarVideo(Path ruta);
    public void detenerReproduccion();
    
    public void inicializarArbolVisual(TreeView<String> arbolVisual);
    public void establecerDirectorioActual(List<Path> directorio);
    
}
