/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectogestiondocumental.modelo;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.util.Duration;

/**
 *
 * @author andy
 */
public class ConfiguracionesImpl implements Configuraciones{
    private Pane base;
    private ReconocimientoVisual reconocimientoVisual;
    private ScrollPane contenedor;
    private ImageView visualizador;
    private TextArea notepad;
    private MediaView reproductorVideo;
    private MediaPlayer mediaPlayer;
    private Slider slider;
    private Button control;
    private TreeView<String> arbolVisual;
    
    private double factor = 0.2;
    private final double DELTA = 0.05;
    private int paginaActual = 1;
    private int paginasTotales = 1;
    
    
    @Override
    public void inicializacion(ScrollPane contenedor, Pane base) {
        this.contenedor = contenedor;
        this.base = base;
       //this.contenedor.setPannable(false);
    }

    private void centrar(){
        // Centrando vista del contenedor Scroll
        contenedor.setHvalue(0.5);
        contenedor.setVvalue(0.5);
        
    }
    
    private ContextMenu menuContexto(ImageView nodo){
       ContextMenu contextMenu = new ContextMenu();
       CheckMenuItem item1 = new CheckMenuItem("Sistema OCR");
       contextMenu.getItems().add(item1); 
       
       item1.setOnAction((event) -> {
           System.out.println("clickeado item: "+item1.isSelected());
           if(item1.isSelected()){
               this.contenedor.setPannable(false); // Desactivando el paneable del scrollpane
               reconocimientoVisual.activarRecortarSeccion(true); // Activando las herramientas de recorte de imagen
           }else {
               this.contenedor.setPannable(true); // Activando el modo paneable del scrollpane
               reconocimientoVisual.activarRecortarSeccion(false); // Desactivando las herramientas de recorte de imagen
           }
           
       });
       
       
       return contextMenu;
    }
    
    @Override
    public void visorImagenes() {
        visualizador = new ImageView();
        
        visualizador.setOnScroll((event) -> {
            
            if(event.getDeltaX() > 0 || event.getDeltaY() > 0) factor += DELTA;
            if(event.getDeltaX() < 0 || event.getDeltaY() < 0) factor -= DELTA;
            
            if(factor <= 0.1) factor = 0.1; 
            if(factor >= 2.0) factor = 2.0;
            
            visualizador.setScaleX(factor);
            visualizador.setScaleY(factor);
            
            //ystem.out.println("Moviendo la rueda-> "+factor);
        });
       
       // Agregando el menu contextual
       ContextMenu emergente = this.menuContexto(visualizador);
       
       // Solicitando presencia del menu contextual
       visualizador.setOnContextMenuRequested((event) -> {
           System.out.println("requerido menu contextual");
           emergente.show(visualizador.getScene().getWindow(), event.getScreenX(), event.getScreenY());
           
       });
       
       // Agregando los procedimientos para la extraccion de porciones de imagenes
       reconocimientoVisual = new ReconocimientoVisualImpl();
       
       reconocimientoVisual.incializar(visualizador, (Pane) contenedor.getParent());
       
        
       contenedor.setContent(visualizador);
    }

    @Override
    public void actualizarImagen(Image imagen) {
       
        visualizador.setImage(null);
        visualizador.setImage(imagen);
        factor = 0.2;
        
        visualizador.setX(0);
        visualizador.setY(0);
        
        visualizador.setLayoutX(0);
        visualizador.setLayoutY(0);
        
        visualizador.setFitWidth(imagen.getWidth());
        visualizador.setFitHeight(imagen.getHeight());
        visualizador.setSmooth(true);
        visualizador.setPreserveRatio(false);
        
        visualizador.setScaleX(factor);
        visualizador.setScaleY(factor);
        
        this.centrar();
        
//        System.out.println("imagen dimension: "+imagen.getWidth()+" - "+imagen.getHeight());
//        System.out.println("imagen FitAncho: "+visualizador.getFitWidth());
//        System.out.println("imagen FitAlto: "+visualizador.getFitHeight());
//        System.out.println("imagen Escalas: "+visualizador.getScaleX()+", "+visualizador.getScaleY());
//        
//        
//        
//        System.out.println("imagenVmax: "+contenedor.getVmax());
//        System.out.println("imagenHmax: "+contenedor.getHmax());
//        System.out.println("imagenH: "+contenedor.getHvalue());
//        System.out.println("imagenV: "+contenedor.getVvalue());
        
    }
    
    
    @Override
    public void establecerPaginacion(int paginasTotales){
      this.paginasTotales = paginasTotales;
      paginaActual = 1;
    }
    
    @Override
    public int[] nroPagina(String texto){
        if(texto.startsWith("+")){ System.out.println("+");
            paginaActual += 1;
            if(paginaActual >= paginasTotales) paginaActual = paginasTotales;
        }else{ System.out.println("-");
            paginaActual -= 1;
            if(paginaActual <= 1) paginaActual = 1;
        }
        
//        System.out.println("pagina: "+paginaActual);
        return new int[]{paginaActual, paginasTotales};
    }
   
    public void liberarVisualizador(){
        visualizador.setImage(null);
    }
   
    @Override
    public void notepad() {
        notepad = new TextArea();
        notepad.setWrapText(true);
        notepad.setEditable(false);
//        notepad.addEventFilter(ContextMenuEvent.CONTEXT_MENU_REQUESTED, (event) -> {
//            event.consume();
//        });

        //MenuItem abrir = new MenuItem("Abrir");
        //notepad.getContextMenu().getItems().add(abrir);
////        System.out.println("Elementos: "+notepad.contextMenuProperty().toString());
        contenedor.setContent(notepad);
    }

    @Override
    public void actualizarNotepad(String texto) {
        notepad.setText(texto);
    }
    
    private void listenerBotonControl(){
        EventHandler<MouseEvent> eventoClick = (event) -> {
            if(control.getText().equalsIgnoreCase("||")){
               mediaPlayer.pause();
               control.setText("|>");
            }else{
               mediaPlayer.play();
               control.setText("||");
            }
        };
        
        control.addEventHandler(MouseEvent.MOUSE_CLICKED, eventoClick);
    }

    @Override
    public void reproductor() {
        reproductorVideo = new MediaView();
        // Estableciendo parametros inciales del reproductor de video
        reproductorVideo.setPreserveRatio(false);
        reproductorVideo.setFitWidth(contenedor.getPrefWidth());
        reproductorVideo.setFitHeight(contenedor.getPrefHeight());
        
        
        AnchorPane panel = new AnchorPane();
        panel.setPrefSize(contenedor.getPrefWidth(), contenedor.getPrefHeight());
        
        // Slider para adelantar y retroceder video
        slider = new Slider(0.0, 1.0, 0.0);
        slider.setPrefSize(350, 16);
        slider.setLayoutX(700);
        slider.setLayoutY(14);
        
        // Boton para reproducir y detener reproduccion
        control = new Button("||");
        control.setPrefSize(50, 25);
        control.setLayoutX(850);
        control.setLayoutY(30);
        control.setStyle("-fx-background-color: linear-gradient(to bottom, rgb(174,188,191) 0%,rgb(110,119,116) 50%,rgb(10,14,10) 51%,rgb(10,8,9) 100%);");
        this.listenerBotonControl();
        
        
        panel.getChildren().add(reproductorVideo);
        panel.getChildren().add(slider);
        panel.getChildren().add(control);
        
        contenedor.setContent(panel);
    }

    @Override
    public void actualizarVideo(Path ruta) {
        this.detenerReproduccion();
        
        Media media = new Media(ruta.toUri().toString());
        
        mediaPlayer = new MediaPlayer(media);
        mediaPlayer.setAutoPlay(true);
        //mediaPlayer.setStartTime(new Duration(25000));
        
        // Creando el listener que observara la reproduccion del video
       ChangeListener<Duration> reproduccionEvento = new ChangeListener<Duration>() {
            @Override
            public void changed(ObservableValue<? extends Duration> observable, Duration oldValue, Duration newValue) {
               // System.out.println("actual: "+newValue);
                slider.setValue(newValue.toMillis());
            }
        };
                
       // Se agrega el listener
       mediaPlayer.currentTimeProperty().addListener(reproduccionEvento);
        
        // Se inicializa los valores del slider apenas empieza la reproduccion
        mediaPlayer.setOnPlaying(() -> {
            System.out.println("tiempo: "+mediaPlayer.currentTimeProperty().get().toString());
            System.out.println("total: "+mediaPlayer.totalDurationProperty().get().toMillis());
            
            slider.setMax(mediaPlayer.totalDurationProperty().get().toMillis());
            slider.setValue(mediaPlayer.currentTimeProperty().get().toMillis());
            
        });
        
        // Cuando se da click al slider se elimina el listener de la reproduccion para asi poder desplazar el slider
        slider.setOnMousePressed((event) -> {
            //System.out.println("presionado");
            mediaPlayer.currentTimeProperty().removeListener(reproduccionEvento);
        });
        
        // Cuando termina el click del mouse sobre el slider se establece el nuevo tiempo del video y se establece nuevamente el listener de reproduccion
        slider.setOnMouseClicked((event) -> {
            //System.out.println("clicked");
            mediaPlayer.seek(new Duration(slider.getValue()));
            mediaPlayer.currentTimeProperty().addListener(reproduccionEvento);
        });
        
        // Se reproduce el video
        reproductorVideo.setMediaPlayer(mediaPlayer);
        this.centrar();
//        System.out.println("videoVmax: "+contenedor.getVmax());
//        System.out.println("videoHmax: "+contenedor.getHmax());
//       System.out.println("videoH: "+contenedor.getHvalue());
//       System.out.println("videoV: "+contenedor.getVvalue());
        
    }

    @Override
    public void detenerReproduccion() {
        if(mediaPlayer != null){
            //mediaPlayer.stop();
            mediaPlayer.dispose();
        }
            
    }

    @Override
    public void inicializarArbolVisual(TreeView<String> arbolVisual) {
        this.arbolVisual = arbolVisual;
    }

    private void crearArbol(List<Path> rutas){
        final int ANCHOALTO = 14;
        
       // Raiz del arbol
       TreeItem<String> directorioBase = new TreeItem<>();
       directorioBase.setValue(rutas.get(0).getFileName().toString());
       directorioBase.setGraphic(
           new ImageView(    
               new Image(getClass().getResourceAsStream("/proyectogestiondocumental/recursos/carpeta16x16.jpg"), 
                               ANCHOALTO, 
                               ANCHOALTO,
                               false,
                               true)
           )
       );
       directorioBase.setExpanded(true);
       
       // Ramas del arbol
       IntStream.range(1, rutas.size()).forEach(indice -> {
           // Separando directorios y archivos
          Image imagen = null;
          
          // Agregando los iconos a directorios y archivos
          if(Files.isDirectory(rutas.get(indice))){ //System.out.println("directorio");
              imagen = //new Image(getClass().getResourceAsStream("/proyectogestiondocumental/recursos/carpeta16x16.jpg"));
                       new Image(getClass().getResourceAsStream("/proyectogestiondocumental/recursos/carpeta16x16.jpg"), 
                               ANCHOALTO, 
                               ANCHOALTO,
                               false,
                               true);
          }else{ //System.out.println("Archivo");
              imagen = //new Image(getClass().getResourceAsStream("/proyectogestiondocumental/recursos/archivo-file16x16.jpg"));
                       new Image(getClass().getResourceAsStream("/proyectogestiondocumental/recursos/file32x32.png"), 
                               ANCHOALTO, 
                               ANCHOALTO,
                               false,
                               true);
          } 
          
          // Agregando los elementos hijos al directorio raiz
          directorioBase.getChildren().add( new TreeItem<>(rutas.get(indice).getFileName().toString(), new ImageView(imagen)) );
           
       });
       
        // Agregando el directorio raiz con su contenido al entorno visual
        this.arbolVisual.setRoot(directorioBase);
    }
    
    private List<Path> ordenarArchivos(List<Path> rutas){
       return rutas.stream().sorted().collect(Collectors.toList());
    }
    
    @Override
    public void establecerDirectorioActual(List<Path> rutas) {
        
        this.crearArbol(this.ordenarArchivos(rutas));
    }
    
    
    
    
    
    
}
