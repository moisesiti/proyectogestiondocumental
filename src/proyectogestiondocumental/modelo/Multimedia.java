/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectogestiondocumental.modelo;

import java.nio.file.Path;

/**
 *
 * @author andy
 */
public interface Multimedia {
    
    public void cargarArchivo(Path ruta);
    public String tipoArchivo();
    
}
