/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectogestiondocumental.modelo;

import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

/**
 *
 * @author andy
 */
public interface ReconocimientoVisual {
    
    public void incializar(ImageView visualizador, Pane panel);
    public void activarRecortarSeccion(boolean opcion);
    public void imagen2Texto();
    
}
