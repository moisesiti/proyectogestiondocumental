/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectogestiondocumental.modelo;

import com.aspose.ocr.ImageStream;
import com.aspose.ocr.ImageStreamFormat;
import com.aspose.ocr.OcrEngine;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javax.imageio.ImageIO;


/**
 *
 * @author andy
 */
public class ReconocimientoVisualImpl implements ReconocimientoVisual{
    private Archivo archivo;
    
    private Rectangle cuadro;
    private ImageView visualizador;
    private Pane panel;
    private int origenX,
                origenY,
                finX,
                finY;
            
    private EventHandler<MouseEvent> mousePresionado = null,
                                     mouseDragg = null,
                                     mouseLiberado = null,
                                     panelPresionado = null, panelDragg = null, panelLiberado = null;
    
    private WritableImage writableImage;
    
    private OcrEngine ocrEngine;
    
    
    @Override
    public void incializar(ImageView visualizador, Pane panel) {
        this.visualizador = visualizador;
        this.panel = panel;
        
       /* System.out.println("id-> "+panel.getId());
        panel.getChildren().forEach(x->{System.out.println("hijo:"+x.toString());});
        
        System.out.println("id2-> "+panel.getParent().getId());
        panel.getParent().getChildrenUnmodifiable().forEach(x->{System.out.println("hijo:"+x.toString());});
        
        System.out.println("id3-> "+panel.getParent().getParent().getId());
        panel.getParent().getParent().getChildrenUnmodifiable().forEach(x->{System.out.println("hijo:"+x.toString());});*/
      
        
        ocrEngine = new OcrEngine();
       
    }

    
     private void exportarImagenArchivo(WritableImage lienzo){
        File outFile = new File("/home/andy/Documentos/imageops-snapshot.png");
                       //  new File("C:\\Users\\Rosa\\Pictures\\imageops-snapshot.png");
                       //new File("C:\\Users\\Andy\\Pictures\\imageops-snapshot.png");
        
        
         try {
            //ImageIO.write(img, "jpg", new File("/home/andy/Documentos/imagen-prueba.jpg"));
            ImageIO.write(SwingFXUtils.fromFXImage(lienzo, null),
                    "png", outFile);
        } catch (IOException ex) {
            Logger.getLogger(ReconocimientoVisualImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
         
         System.out.println("Imagen Exportada: "+outFile.getPath());
    }
     
    private void snapshotEscena(){
      WritableImage wi = visualizador.getScene().snapshot(null);
      File outFile = new File("/home/andy/Documentos/imageops-snapshot.png");
        
      this.exportarImagenArchivo(wi);
      
            
//            Robot robot = null;
//        try {
//            robot = new Robot();
//        } catch (AWTException ex) {
//            Logger.getLogger(ReconocimientoVisualImpl.class.getName()).log(Level.SEVERE, null, ex);
//        }
//            BufferedImage img = robot.createScreenCapture(
//                    new java.awt.Rectangle(
//                            (int) event.getScreenX(), 
//                            (int) event.getScreenY(),
//                            (int) cuadro.getWidth(), 
//                            (int) cuadro.getHeight()
//                    )
//            );
//        try {
//            ImageIO.write(img, "jpg", new File("/home/andy/Documentos/imagen-prueba.jpg"));
//        } catch (IOException ex) {
//            Logger.getLogger(ReconocimientoVisualImpl.class.getName()).log(Level.SEVERE, null, ex);
//        }
      
    }
    
   
    @Override
    public void imagen2Texto() {
       
        System.out.println("Conviritiendo imagen seleccionada a texto");
        /*
            URL url = new URL("http://www.google.com/intl/en_ALL/images/logo.gif");
            BufferedImage image = ImageIO.read(url);
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write(image, "gif", os);
            InputStream is = new ByteArrayInputStream(os.toByteArray());
        */
        String textoRecuperado = null;
        BufferedImage bufferImagen = SwingFXUtils.fromFXImage(writableImage, null);
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        
        try {
            ImageIO.write(bufferImagen, "png", os);
            InputStream is = new ByteArrayInputStream(os.toByteArray());
            
            //ocrEngine.setImage(ImageStream.fromStream(new FileInputStream("/home/andy/Documentos/imageops-snapshot.png"), ImageStreamFormat.Png));
            ocrEngine.setImage(ImageStream.fromStream(is, ImageStreamFormat.Png));
                    
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReconocimientoVisualImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReconocimientoVisualImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(ocrEngine.process())
           // System.out.println("RESPUESTA TEXTO: " + ocrEngine.getText().toString());
            textoRecuperado = ocrEngine.getText().toString();
        //-------------------------------------------------------------------------------
        
     
        this.setClipboard(textoRecuperado);
        new VentanaImpl().mensajeria("Extracción OCR", "Texto: "+textoRecuperado+"\ncopiado al portapeles");
        
        System.out.println("textoExtraido: "+textoRecuperado);
    }
    
    
    private void recorte(){ System.out.println("trozo = origenX:"+origenX+" origenY:"+origenY+" | finX:"+finX+" finY:"+finY);
        Image trozo = visualizador.getImage();
        PixelReader pixelReader = trozo.getPixelReader();
        int ancho = finX - origenX,
            alto = finY - origenY;
        
        System.out.println("iniciando exportacion");
     
        writableImage = new WritableImage(ancho, alto);
        PixelWriter pw = writableImage.getPixelWriter();
        
        IntStream.range(origenX, finX).forEach(posX -> {
            IntStream.range(origenY, finY).forEach(posY ->{
                int coordX = posX - origenX,
                    coordY = posY - origenY;    
                
                Color colorPixel = pixelReader.getColor(posX, posY);
                pw.setColor(coordX, coordY, colorPixel);
                
              //  System.out.println("valor: ("+posX+","+posY+") = "+colorPixel.toString());
                
            });
        });
        
        // Exportando imagen fisicamente
        //this.exportarImagenArchivo(writableImage);
        
        // Convirtiendo la imagen a texto
        this.imagen2Texto();
        
        System.out.println("imagen traducida");
    }
   
    private void agregarCuadro(){
        cuadro = new Rectangle();
        cuadro.setStyle("-fx-fill: rgba(182,224,38,0.6);");
        Pane panelBase = (Pane) panel.getParent().getParent();
        panelBase.getChildren().add(cuadro);
    }
    
    private void removerCuadro(){
        Pane panelBase = (Pane) panel.getParent().getParent();
        panelBase.getChildren().remove(cuadro);
    }
    
    @Override
    public void activarRecortarSeccion(boolean opcion) {System.out.println("recorte");
        //System.out.println("dimension: ancho:"+visualizador.getImage().getWidth()+" alto:"+visualizador.getImage().getHeight());
        //System.out.println("imagen: "+visualizador.getImage().getPixelReader().getPixelFormat().getType().toString());

        
        if(opcion){ System.out.println("Activando herramientas para recorte visual");
            mousePresionado = (event) -> {
                if(event.isPrimaryButtonDown()){// Solo con el boton izquierdo del mouse
                    System.out.println("mousePresionadoRecorte");
                    // Estableciendo origen con respecto a la escena
                    this.agregarCuadro();
                    cuadro.setLayoutX(event.getSceneX());
                    cuadro.setLayoutY(event.getSceneY());
                    
                    // Extraido del visualizador de imagen    
                    origenX = (int) event.getX();
                    origenY = (int) event.getY();
                    
                 }
            }; 
            
            mouseDragg = (event) -> {
                 if(event.isPrimaryButtonDown()){// Solo con el boton izquierdo del mouse
                     //System.out.println("dragg: "+event.getX()+" "+event.getY());   
                     /*cuadro.setWidth( event.getSceneX() - cuadro.getLayoutX());
                     cuadro.setHeight( event.getSceneY() - cuadro.getLayoutY());*/
                     
                     cuadro.setWidth( event.getSceneX() - cuadro.getLayoutX());
                     cuadro.setHeight( event.getSceneY() - cuadro.getLayoutY());
                     
                     //System.out.println("ancho "+cuadro.getWidth()+" alto:"+cuadro.getHeight());
                    }
            };
            
            mouseLiberado = (event) -> {
                System.out.println("MouseRecorteliberado "+event.getX()+", "+event.getY());

                    finX = (int) event.getX();
                    finY = (int) event.getY();

                    Pane panelBase = (Pane) panel.getParent().getParent();
                    if(panelBase.getChildren().contains(cuadro)){ // Solo si el cuadro existe cuando se libera el boton del raton
                       this.recorte();
                       this.removerCuadro();
                    }
            };
            
            
            // Agregando los listener al elemento de interes
            visualizador.addEventHandler(MouseEvent.MOUSE_PRESSED, mousePresionado);
            visualizador.addEventHandler(MouseEvent.MOUSE_DRAGGED, mouseDragg);
            visualizador.addEventHandler(MouseEvent.MOUSE_RELEASED, mouseLiberado);

            
        }else{System.out.println("Desactivando herramientas para recorte visual");
            
            visualizador.removeEventHandler(MouseEvent.MOUSE_PRESSED, mousePresionado);
            visualizador.removeEventHandler(MouseEvent.MOUSE_DRAGGED, mouseDragg);
            visualizador.removeEventHandler(MouseEvent.MOUSE_RELEASED, mouseLiberado);
         
        }       
      
    }

    // Método que inserta en el portapapeles una cadena.
    public void setClipboard(String texto){ System.out.println("Copiando al portapapeles");
        StringSelection ss = new StringSelection(texto);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, ss);
     }
    
    public String getClipboard(){ System.out.println("Extraccion del portapapeles");
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        Transferable data = clipboard.getContents(this);
         String informacion = null;
        
        try {
            informacion = (String) data.getTransferData(DataFlavor.stringFlavor);
        } catch (UnsupportedFlavorException | IOException ex) {
            Logger.getLogger(ReconocimientoVisualImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        /*if ( t.isDataFlavorSupported(DataFlavor.stringFlavor) )
        {
            Object o = t.getTransferData( DataFlavor.stringFlavor );
            String data = (String)t.getTransferData( DataFlavor.stringFlavor );
            System.out.println( "Clipboard contents: " + data );
        }*/
        
     return informacion;   
    }
    
    
 
    
}
