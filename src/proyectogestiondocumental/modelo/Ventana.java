/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectogestiondocumental.modelo;

import java.nio.file.Path;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;

/**
 *
 * @author andy
 */
public interface Ventana {
    
    public Parent cargarFXML(String rutaFXML);
    public void establecerFoco(Node nodo);
    public Node buscarNodo(Parent nodoBase, String nombreNodo);
    
    public void mensajeria(String titulo, String contenido);
    public Label mensajeriaAutomatizada(String titulo, String contenido);
    public String seleccionarDirectorio(String titulo);
    public String seleccionarDirectorio(String titulo, Path directorioOrigen);
    
    public void nuevaVentana(String rutaFXML);
    public void cerrarVentana(Node nodo);
    
    public String confirmacion(String titulo, String pregunta, String textoBtn1, String textobtn2);
    public Path seleccionarArchivo(String titulo);
}