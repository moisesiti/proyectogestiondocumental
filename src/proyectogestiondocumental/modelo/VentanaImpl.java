/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectogestiondocumental.modelo;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Modality;
import javafx.stage.Popup;
import javafx.stage.PopupWindow;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author andy
 */
public class VentanaImpl implements Ventana{
    private File directorioInicial;
    
    public VentanaImpl() {
        directorioInicial = Paths.get(System.getProperty("user.home")).toFile();
    }
    
    

    @Override
    public Parent cargarFXML(String rutaFXML) {
        // /stdxml/vista/auxiliares/Modelo1.fxml
        Parent root = null;
       // FXMLLoader loader = null;
        
                
        try {
            
          // loader = new FXMLLoader(getClass().getResource(rutaFXML)).load();
           root = FXMLLoader.load(getClass().getResource(rutaFXML));
          // root = loader.load();
           
           
        } catch (IOException ex) {
            Logger.getLogger(VentanaImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return root;
    }

    @Override
    public void establecerFoco(Node nodo) {
       Platform.runLater(new Runnable() {
            @Override
            public void run() {
                nodo.requestFocus();
            }
        });
    }

    @Override
    public Node buscarNodo(Parent nodoBase, String nombreNodo) {
      return
        nodoBase.getChildrenUnmodifiable()
                .stream()
                .filter(nodo -> {
                  // System.out.println("nodoPlus: "+nodo.get);
                  Optional<String> elemento = Optional.ofNullable(nodo.getId());
                  boolean encontrado = false;
                  
                  encontrado = elemento.isPresent() ? elemento.get().equalsIgnoreCase(nombreNodo) : false;
                    //System.out.println("nodo: "+nodo.getId()+" existe: "+encontrado);
                  return encontrado;
                        })
                .findFirst()
                .orElse(null);
    }

    private void agregarEventosSalida(Node nodo){
        
        
        nodo.setOnMouseClicked(new EventHandler<MouseEvent>() {
           @Override
           public void handle(MouseEvent event) {
               //System.out.println("Ejecutando una accion");
               Stage st = (Stage) nodo.getScene().getWindow();
               st.close();
           }
       });
        
       nodo.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
               // System.out.println("letra: "+event.getCode()+" - "+event.getCharacter());
                if(event.getCode() == event.getCode().ESCAPE){
                    Stage st = (Stage) nodo.getScene().getWindow();
                    st.close();
                }
            }
        });
    
    }
    
    private Stage ventanaModal(Parent root){
        final Stage ventana = new Stage(StageStyle.TRANSPARENT);
            
        Scene scene = new Scene(root);
        scene.setFill(null);
       
         
        ventana.initModality(Modality.APPLICATION_MODAL);
        ventana.setScene(scene);
        ventana.setResizable(false);
        ventana.sizeToScene();
        
      return ventana;
    }
    
    
    @Override
    public void mensajeria(String titulo, String contenido) {
        Parent root = this.cargarFXML("/proyectogestiondocumental/vista/emergentes/Mensaje.fxml");
       
       Label tituloNodo = (Label) this.buscarNodo(root, "titulo");
       Label descripcionNodo = (Label) this.buscarNodo(root, "descripcion");
       Button salirNodo = (Button) this.buscarNodo(root, "salir");
       
       tituloNodo.setText(titulo);
       descripcionNodo.setText(contenido);
       
       
       this.agregarEventosSalida(salirNodo);
      
        Platform.runLater(new Runnable() {
           @Override
           public void run() {
                new VentanaImpl().ventanaModal(root).showAndWait();
           }
       });
    }

    @Override
    public Label mensajeriaAutomatizada(String titulo, String contenido) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String seleccionarDirectorio(String titulo) {
        PopupWindow pw = new Popup();
        String salida = null;
    
        DirectoryChooser directorio = new DirectoryChooser();
        directorio.setTitle(titulo);
       
        Optional<File> respuesta = Optional.ofNullable(directorio.showDialog(pw));
        if(respuesta.isPresent()) salida = respuesta.get().toPath().toString();
    
        return salida;
    }

    public String seleccionarDirectorio(String titulo, Path directorioOrigen) {
        PopupWindow pw = new Popup();
        String salida = null;
       
        DirectoryChooser directorio = new DirectoryChooser();
        directorio.setTitle(titulo);
        directorio.setInitialDirectory(directorioOrigen.toFile());
       
        Optional<File> respuesta = Optional.ofNullable(directorio.showDialog(pw));
        if(respuesta.isPresent()) salida = respuesta.get().toPath().toString();
       
        return salida;
    }
    
    @Override
    public Path seleccionarArchivo(String titulo) {
        PopupWindow pw = new Popup();
        Path salida = null;
        
        FileChooser directorio = new FileChooser();

        
        directorio.getExtensionFilters().addAll(
//            new ExtensionFilter("Archivo de Texto", "*.txt"),
//            new ExtensionFilter("Archivo de Documento", "*.doc", "*.docx", "*.odt"),
//            new ExtensionFilter("Archivo de Imagen", "*.png", "*.jpg", "*.gif"),
//            new ExtensionFilter("Archivo de Audio", "*.wav", "*.mp3", "*.aac"),
//            new ExtensionFilter("Archivo de Video", "*.mp4"),
            new ExtensionFilter("Todos los Archivos", "*.*")
        );
        
        
        directorio.setTitle(titulo);
        directorio.setInitialDirectory(directorioInicial);
        
        Optional<File> respuesta = Optional.ofNullable(directorio.showOpenDialog(pw));
        if(respuesta.isPresent()){
            directorioInicial = respuesta.get().toPath().getParent().toFile();
            salida = respuesta.get().toPath();
        }else{
            //Paths.get(System.getProperty("user.home")).toFile()
        }
        
        return salida;
                
    }
    
    

    @Override
    public void nuevaVentana(String rutaFXML) {
       Stage stage = new Stage();
       //stage.initStyle(StageStyle.TRANSPARENT);
                             
     
       Scene scene = new Scene(this.cargarFXML(rutaFXML));
      // scene.setFill(null);
       
       stage.setTitle("Sistema de Gestión de Documentos");
       stage.getIcons().add(new Image(getClass().getResource("/proyectogestiondocumental/recursos/logo.png").toExternalForm()));
      
       
       stage.setScene(scene);
       //stage.setResizable(false);
       stage.sizeToScene();
       stage.show();
    }

    @Override
    public void cerrarVentana(Node nodo) {
       Stage stg = (Stage) nodo.getScene().getWindow();
       stg.close();
    }

    @Override
    public String confirmacion(String titulo, String pregunta, String textoBtn1, String textoBtn2) {
       Parent root = this.cargarFXML("/proyectogestiondocumental/vista/emergentes/Confirmacion.fxml");
       
       // Variable concurrente serializable
       AtomicReference<String> respuesta = new AtomicReference();
       
       
       // Extrayendo los nodos que seran modificados de la vista
       Label tituloNodo = (Label) this.buscarNodo(root, "titulo");
       Label descripcionNodo = (Label) this.buscarNodo(root, "descripcion");
       Button boton1 = (Button) this.buscarNodo(root, "boton1");
       Button boton2 = (Button) this.buscarNodo(root, "boton2");
       
       // Estableciendo los nuevos valores
       tituloNodo.setText(titulo);
       descripcionNodo.setText(pregunta);
       boton1.setText(textoBtn1);
       boton2.setText(textoBtn2);
       
       // Creando la accion de interes
       EventHandler acciones = new EventHandler() {
            @Override
            public void handle(Event event) {
              Button botonClikeado = (Button)event.getSource();
              respuesta.set(botonClikeado.getText());
              
              Stage st = (Stage) root.getScene().getWindow();
              st.close();
            }
        };
       
       // Añadiendo la acción a los elementos de interés
       boton1.setOnAction(acciones);
       boton2.setOnAction(acciones);
       
       // Desplegando la ventana para que espere
       this.ventanaModal(root).showAndWait();
   
       // Esperando respuesta
      return respuesta.get(); 
    }

    
}
